package decorator;

public class Milchschaum extends ZutatDekorierer {
    Getraenk getraenk;

    public Milchschaum(Getraenk getränk) {
        this.getraenk = getraenk;
    }

    public String getBeschreibung(){
        return getraenk.getBeschreibung() + ", Milchschaum";
    }

    public double preis(){
        return .10 + getraenk.preis();
    }
}

