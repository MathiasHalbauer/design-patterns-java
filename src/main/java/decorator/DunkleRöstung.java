package decorator;

public class DunkleRöstung extends Getraenk {

    public DunkleRöstung(){
        beschreibung = "Dunkle Röstung";
    }


    public String getBeschreibung() {
        return this.beschreibung;
    }

    public double preis(){
        return .99;
    }
}
