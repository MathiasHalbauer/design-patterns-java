package decorator;

public class Soja extends Getraenk {
    Getraenk getraenk;

    public Soja(Getraenk getränk){
        this.getraenk = getraenk;
    }

    public String getBeschreibung(){
        return getraenk.getBeschreibung() + ", Soja";
    }

    public double preis(){
        return .15 + getraenk.preis();
    }
}

