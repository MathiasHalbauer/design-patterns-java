package proxy;

public class PersonBeanImpl implements PersonBean {
    String name;

    @Override
    public String toString() {
        return "PersonBeanImpl{" +
                "name= '" + name + '\'' +
                ", geschlecht= '" + geschlecht + '\'' +
                ", interessen= '" + interessen + '\'' +
                ", bewertung= " + bewertung +
                ", bewertungsAnzahl= " + bewertungsAnzahl +
                '}';
    }

    String geschlecht;
    String interessen;
    int bewertung;
    int bewertungsAnzahl = 0;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getGeschlecht() {
        return this.geschlecht;
    }

    @Override
    public String getInteressen() {
        return this.interessen;
    }

    @Override
    public int getHotOrNotBewertung() {
        return this.bewertung;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setGeschlecht(String geschlecht) {
        this.geschlecht = geschlecht;
    }

    @Override
    public void setInteressen(String interessen) {
        this.interessen = interessen;
    }

    @Override
    public void setHotOrNotBewertung(int bewertung) {
        this.bewertung += bewertung;
        this.bewertungsAnzahl++;
    }
}
