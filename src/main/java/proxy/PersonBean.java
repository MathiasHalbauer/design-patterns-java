package proxy;

public interface PersonBean {
    String getName();
    String getGeschlecht();
    String getInteressen();
    int getHotOrNotBewertung();

    void setName(String name);
    void setGeschlecht(String geschlecht);
    void setInteressen(String interessen);
    void setHotOrNotBewertung(int bewertung);
}
