package proxy;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

public class PartnerVermittlungsTestlauf {
    List<PersonBean> db;

    public PartnerVermittlungsTestlauf() {
        initialisiereDatenbank();
    }

    private void initialisiereDatenbank() {
        db = new ArrayList<>();
        PersonBean henri = new PersonBeanImpl();

        henri.setGeschlecht("male");
        henri.setName("Henri Javabean");
        henri.setHotOrNotBewertung(5);
        henri.setInteressen("Schlittschufahren");
        db.add(henri);


    }

    public static void main(String[] args) {
        PartnerVermittlungsTestlauf test = new PartnerVermittlungsTestlauf();
        test.laufen();
    }



    private void laufen() {
        PersonBean Henri = getPersonAusDatenbank("Henri Javabean");
        PersonBean eigentuemerProxy = getEigentuemerProxy(Henri);
        //System.out.println(eigentuemerProxy);
        eigentuemerProxy.setInteressen("Bowling, Schach");
        System.out.println("Interessen durch Eigentümer Proxy gesetzt");

        try {
            eigentuemerProxy.setHotOrNotBewertung(10);
        }
        catch (Exception e){
            System.out.println("Bewertung kann nicht durch Eigentümer Proxy gesetzt werden");

        }
        System.out.println("Bewertung ist " + eigentuemerProxy.getHotOrNotBewertung());

        PersonBean nichtEigentuemerProxy = getNichtEigentuemerProxy(Henri);
        System.out.println("Name ist " + nichtEigentuemerProxy.getName());

        try {
            nichtEigentuemerProxy.setInteressen("Bowling, Schach");

        }
        catch (Exception e){
            System.out.println("Interessen können nicht durch Nicht-Eigentümer-Proxy gesetzt werden");
        }

        nichtEigentuemerProxy.setHotOrNotBewertung(8);
    }


    private PersonBean getEigentuemerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(
                person.getClass().getClassLoader(),
                person.getClass().getInterfaces( ),
                new EigentuemerInvocationHandler(person)
        );

    }

    private PersonBean getNichtEigentuemerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(
                person.getClass().getClassLoader(),
                person.getClass().getInterfaces( ),
                new NichtEigentuemerInvocationHandler(person));

    }

    private PersonBean getPersonAusDatenbank(String name) {
        for (PersonBean person: db){
            if (person.getName().equals(name)){
                return person;
            }

        }
        return null;
    }
}
