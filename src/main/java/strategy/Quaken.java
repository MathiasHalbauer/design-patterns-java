package strategy;

public class Quaken implements QuakVerhalten{

    @Override
    public void quaken() {
        System.out.println("Quak Quak");
    }
}
