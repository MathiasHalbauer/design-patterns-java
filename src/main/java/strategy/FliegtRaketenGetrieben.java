package strategy;

public class FliegtRaketenGetrieben implements FlugVerhalten {
    public void fliegen(){
        System.out.println("Fliegt mit Raketenantrieb");
    }
}
