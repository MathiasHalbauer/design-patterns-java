package strategy;

public abstract class Ente {
    FlugVerhalten  flugVerhalten;
    QuakVerhalten quakVerhalten;

    public Ente(){

    }

    public void tuFliegen(){
        flugVerhalten.fliegen();
    }

    public void tuQuaken(){
        quakVerhalten.quaken();
    }

    public void schwimmen(){
        System.out.println("Alle Enten schwimmen auch Holzenten");
    }

    public void setFlugVerhalten(FlugVerhalten fv){
        this.flugVerhalten = fv;
    }

    public void setQuakVerhalten(QuakVerhalten qv){
        this.quakVerhalten = qv;
    }
}
