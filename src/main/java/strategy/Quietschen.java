package strategy;

public class Quietschen implements QuakVerhalten{
    @Override
    public void quaken() {
        System.out.println("Quietsch");
    }
}
