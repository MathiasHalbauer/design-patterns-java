package strategy;

public class MiniEntenSimulator {
    public static void main(String[] args) {
        Ente mallard = new StockEnte();
        mallard.tuQuaken();
        mallard.tuFliegen();

        Ente modell = new ModellEnte();
        modell.tuFliegen();
        modell.setFlugVerhalten(new FliegtRaketenGetrieben());
        modell.tuQuaken();

    }
}
