# Design Patterns in Java #

Sammlung von Java-Implementierungen verschiedener Design-Patterns aus dem Buch "Entwurfsmuster von Kopf bis Fuß" von O'Reilly (Freeman et al.). Hierbei finden sich Lösungen für
die Programmieraufgaben zur Implementierung im Buch.
 
Hierbei sind aktuell folgende Pattern implementiert:

- Decorator
- Strategy
	
Ausstehend sind:

- Adapter
- Observer
- Template
- Proxy
- Model-View-Controller (kombiniertes Muster aus Strategy, Observer, Composite)
